%global debug_package %{nil}

Name:          libkae
Summary:       Huawei Kunpeng Accelerator Engine
Version:       1.2.10
Release:       5
License:       Apache-2.0
Source:        %{name}-%{version}.tar.gz
Vendor:        Huawei Corporation
ExclusiveOS:   linux
URL:           https://support.huawei.com 
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Prefix:        /usr/local/lib/engines-1.1
Conflicts:     %{name} < %{version}-%{release}
Provides:      %{name} = %{version}-%{release}
BuildRequires: libwd >= %{version} openssl-devel sed
Requires:      openssl
ExclusiveArch: aarch64

Patch0001:     0001-Don-t-redefine-gettid-if-glibc-provides-it.patch
Patch0002:     0002-fix-pthread_yield.patch

%description
This package contains the Huawei Kunpeng Accelerator Engine

%prep
%autosetup -c -n %{name}-%{version} -p1

%build
cd KAE
chmod +x configure
./configure
make

%install
mkdir -p ${RPM_BUILD_ROOT}/usr/local/lib/engines-1.1
install -b -m755 KAE/libkae.so.%{version} ${RPM_BUILD_ROOT}/usr/local/lib/engines-1.1

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(755,root,root)
/usr/local/lib/engines-1.1/libkae.so.%{version}

%pre
if [ "$1" = "2" ] ; then  #2: update
    rm -rf $RPM_INSTALL_PREFIX/kae.so      > /dev/null 2>&1 || true
    rm -rf $RPM_INSTALL_PREFIX/kae.so.0    > /dev/null 2>&1 || true
fi

%post
if [[ "$1" = "1" || "$1" = "2" ]] ; then  #1: install 2: update
    ln -sf $RPM_INSTALL_PREFIX/libkae.so.%{version}    $RPM_INSTALL_PREFIX/kae.so
    ln -sf $RPM_INSTALL_PREFIX/libkae.so.%{version}    $RPM_INSTALL_PREFIX/kae.so.0
fi
/sbin/ldconfig

%preun
if [ "$1" = "0" ] ; then  #0: uninstall
    rm -rf $RPM_INSTALL_PREFIX/kae.so   > /dev/null 2>&1 || true
    rm -rf $RPM_INSTALL_PREFIX/kae.so.0 > /dev/null 2>&1 || true
    rm -f /var/log/kae.log              > /dev/null 2>&1 || true
    rm -f /var/log/kae.log.old          > /dev/null 2>&1 || true
fi

%postun
/sbin/ldconfig

%changelog
* Thu Aug 12 2021 caodongxia <caodongxia@huawei.com> 1.2.10-5
- Fix pthread_yield is deprecated

* Tue Jul 28 2020 lingsheng <lingsheng@huawei.com> 1.2.10-4
- Check glibc version to avoid redefine gettid()

* Sun Mar 15 2020 zhangtao <zhangtao221@huawei.com> 1.2.10-3
- Specify aarch64 compilation

* Tue Mar 03 2020 catastrowings <jianghuhao1994@163.com> 1.2.10-2
- openEuler init

* Tue Jan 07 2020 jinbinhua <jinbinhua@huawei.com> 1.2.7-1
- First Spec Version Include kunpeng_engine Code
